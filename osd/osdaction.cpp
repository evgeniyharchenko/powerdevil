/*
    SPDX-FileCopyrightText: 2016 Sebastian Kügler <sebas@kde.org>

    Work sponsored by the LiMux project of the city of Munich:
    SPDX-FileCopyrightText: 2018 Kai Uwe Broulik <kde@broulik.de>

    SPDX-FileCopyrightText: 2022 David Redondo <kde@david-redondo.de>
    SPDX-FileCopyrightText: 2023 Natalie Clarius <natalie.clarius@kde.org>


    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include "osdaction.h"

#include <KLocalizedString>
#include <KIconColors>
#include <KIconEngine>
#include <KIconLoader>
#include <Plasma/Theme>

using namespace PowerDevil;

QList<OsdAction> OsdAction::availableActions()
{
    return {
        {QStringLiteral("power-saver"), i18nc("power profile", "Power Save Mode"), QIcon(new KIconEngine(QStringLiteral("battery-profile-powersave"), KIconColors(Plasma::Theme::globalPalette()), KIconLoader::global()))},
        {QStringLiteral("balanced"), i18nc("power profile", "Balanced Performance Mode"), QIcon(new KIconEngine(QStringLiteral("speedometer"), KIconColors(Plasma::Theme::globalPalette()), KIconLoader::global()))},
        {QStringLiteral("performance"), i18nc("power profile", "Maximum Performance Mode"), QIcon(new KIconEngine(QStringLiteral("battery-profile-performance"), KIconColors(Plasma::Theme::globalPalette()), KIconLoader::global()))},
    };
}

#include "moc_osdaction.cpp"
